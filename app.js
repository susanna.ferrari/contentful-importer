var data = require('./data.json');
var http = require('http-request');
var querystring = require('querystring');
var async = require('async');

const spaceId = 'pim04h4wrha8';
const accessToken = 'CFPAT-7c5e28d40267556c105fdfc17fb8ba0108443503d03962464bef77c22e26ca4e';
const sleep = ms => {
  return new Promise(resolve => setTimeout(resolve, ms));
};

var languageMap = {de: 'de', en: 'en-US', fr: 'fr-FR', it: 'it-IT', nl: 'nl-NL', es: 'es-ES'};

console.log('Import started.');
var cont = 1;

async.eachSeries(
  Object.keys(data),
  function(translationGroup, callbackFinal) {
    async.eachSeries(
      Object.keys(data[translationGroup]),
      function(key, callback) {
        var translations = data[translationGroup][key];

        var translationsByLanguage = {};

        translations.forEach(translation => {
          var language = languageMap[translation.language];
          if (language) {
            translationsByLanguage[language] = translation.name;
          }
        });

        if (Object.keys(translationsByLanguage).length <= 0) {
          callback();
          return;
        }

        var postBody = {
          fields: {
            key: {
              'en-US': key,
            },
            value: translationsByLanguage,
          },
        };

        http.post(
          {
            url: `https://api.contentful.com/spaces/${spaceId}/entries?access_token=${accessToken}`,
            reqBody: new Buffer(JSON.stringify(postBody)),
            headers: {
              'Content-Type': 'application/json',
              'X-Contentful-Content-Type': 'translation',
            },
          },
          (err, res) => {
            if (!err) {
              console.log('Added translation with key ' + key);
              cont++;
              callback();
            } else {
              callback(err);
            }
          }
        );
      },
      function(err) {
        // if any of the file processing produced an error, err would equal that error
        if (!err) {
          console.log(`${cont} translations imported.`);
          callbackFinal();
        } else {
          console.error(err.message);
          callbackFinal(err);
        }
      }
    );
  },
  function(err) {
    // if any of the file processing produced an error, err would equal that error
    if (!err) {
      console.log(`${cont} translations imported.`);
    } else {
      console.error(err.message);
    }
  }
);
